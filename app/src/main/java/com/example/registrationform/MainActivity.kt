package com.example.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var email: EditText
    lateinit var password: EditText
    lateinit var name: EditText
    lateinit var surname: EditText
    lateinit var check: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        setContentView(R.layout.activity_main)

        email = findViewById(R.id.emailText)
        password = findViewById(R.id.passwordText)
        name = findViewById(R.id.nameText)
        surname = findViewById(R.id.surnameText)
        check = findViewById(R.id.check)

    }

    fun register(clickedView: View) {

        if (email.text.isNotEmpty() && name.text.isNotEmpty() && surname.text.isNotEmpty() && password.text.isNotEmpty()) {

            if ("@" in email.text) {

                if (check.isChecked) {

                   if (name.text.length>3) {
                       if (surname.text.length>5){
                           Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()
                       }else{
                           Toast.makeText(applicationContext, "minimum 5", Toast.LENGTH_SHORT).show()
                       }

                   }else {
                       Toast.makeText(applicationContext, "minimum 3", Toast.LENGTH_SHORT).show()
                   }

                } else {

                    Toast.makeText(applicationContext, "Check License... :d", Toast.LENGTH_SHORT).show()

                }

            } else {
                Toast.makeText(applicationContext, "Invalid email", Toast.LENGTH_SHORT).show()
            }

        } else {

            Toast.makeText(applicationContext, "Please fill form.", Toast.LENGTH_SHORT).show()

        }

    }

}